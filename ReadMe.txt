Installation:
-------------

    Place the "mmmr" folder, with its contents, in the public area of a Web server,
where PHP 5.3 can be run. In a browser, navigate to the form, 
"http://<Web server root>/mmmr/form.html". To test, enter the JSON from the first
example below. If building your own HTML Form, make sure that the "action" attribute
is "http://<Web server root>/mmmr/" and the "method" attribute is "POST".

As of 8 Jan 2014 @ 9:30 a.m. EST, there is a working example of this endpoint at
"http://latinst.org/newsite/mmmr/form.html".


Expected Functionality:
-----------------------

    The mmmr endpoint expects a POSTed JSON array of numbers, and will respond with
JSON containing the mean, median, mode, and range of that list. A sample POST request
may look like this:

    {"numbers": [5, 6, 5, 6, 8, 7.9, 8, 9, 8, 5, 11]}

The array, "numbers", is expected. The above request would result in the following 
response from the endpoint:

    {"results":{"mean":7.173,"median":7.9,"mode":[5,8],"range":6}}  

Note that the "mode" above contains two values. Both 5 and 8 repeat three times.
(6 is not included in the mode, because it repeats only two times.)

Missing Values:
---------------

    Given a valid JSON request, it is possible to encounter responses without values
for "median", "mode", and "range":

    {"numbers": [5, 6, 5, 6, 8, 7.9, 8, 9, 8, 5]} results in 
    {"results":{"mean":6.79,"median":"","mode":[5,8],"range":4}}.

    {"numbers": [2.1, 5, 6, 8, 7.9, 9, 11]} results in 
    {"results":{"mean":7,"median":7.9,"mode":"","range":8.9}} 

Each of "median", "mode", and "range" depend upon sorting the input array of numbers.
Should that sort fail, for any reason, then "" will be reported for the problematic
value, along with an error message in the JSON. Although this is unlikely to happen,
here is an example:

    {"results":{"mean":7,"median":7.9,"mode":"","range":"",
     "errors":"Error sorting numbers and determining range."}}

Other Errors:
-------------

Requests made to the "mmmr" endpoint other than POST will generate a response
like the following:

    {"error":{"code":404,
     "message":"Not Found: Method GET is not available on this endpoint."}}  

As mentioned before, a "numbers" array must appear in the JSON:

    {"nums": [2.1, 5, 6, 8, 7.9, 9, 11]} results in
    {"error":{"code":400,
     "message":"Invalid Request: Value POSTed does not contain a 'numbers' array."}} 

An empty array is invalid:

    {"numbers": []} results in
    {"error":{"code":400,
     "message":"Invalid Request: Array POSTed contains no values."}} 

Non-numeric values will cause an error:

    {"numbers": [2.1, 5, 6, "XOXOXO", 7.9, 9, 11]} results in
    {"error":{"code":400,
     "message":"Invalid Request: Array POSTed contains non-numeric values."}} 

Although unlikely, it is possible that an error will develop when executing the page
on the server like the following:

{"error":{"code":500,
 "message":"System Error: The following problem resulted from your request: Object access corrupted."}} 


