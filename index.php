<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<?php
//Request array:
$aryRequest = array();
//Response array:
$aryResponse = array();
//MMMR object:
$objMMMR = new MMMR();

switch($_SERVER['REQUEST_METHOD']){
   case 'POST': 
      try{
         //This loop allows the POSTed variable to have any name 
         //We assume that it has one element, an array called 'numbers':
         foreach($_POST as $element){
            $aryRequest = json_decode(urldecode($element))->numbers;
         }
         //Expected error conditions:
         if(is_array($aryRequest) == false){
            $aryResponse['error']['code'] = 400;
            $aryResponse['error']['message'] = 'Invalid Request: Value POSTed does not contain a \'numbers\' array.';
         
         }elseif($objMMMR->isArrayFull($aryRequest) == false){
            $aryResponse['error']['code'] = 400;
            $aryResponse['error']['message'] = 'Invalid Request: Array POSTed contains no values.';
          
         }elseif($objMMMR->isArrayNumeric($aryRequest) == false){
            $aryResponse['error']['code'] = 400;
            $aryResponse['error']['message'] = 'Invalid Request: Array POSTed contains non-numeric values.';

         //Handle request:
         }else{
            $aryResponse['results']['mean'] = $objMMMR->nullToString($objMMMR->getMean($aryRequest));
            $aryResponse['results']['median'] = $objMMMR->nullToString($objMMMR->getMedian($aryRequest));
            $aryResponse['results']['mode'] = $objMMMR->nullToString($objMMMR->getMode($aryRequest));
            $aryResponse['results']['range'] = $objMMMR->nullToString($objMMMR->getRange($aryRequest));
            if($objMMMR->getErrMsg() != ''){
               $aryResponse['results']['errors'] = $objMMMR->getErrMsg();
            }
         }  
      //Unexpected error:
      }catch(Exception $e){
         $aryResponse['error']['code'] = 500;
         $aryResponse['error']['message'] = 'System Error: The following problem resulted from your request: '.
            $e->getMessage();
      }
      break;
   //Request method not POST:   
   default:
      $aryResponse['error']['code'] = 404;
      $aryResponse['error']['message'] = 'Not Found: Method '.$_SERVER['REQUEST_METHOD'].
         ' is not available on this endpoint.';
}//End switch

//Echo the JSON response:
echo json_encode($aryResponse);

?>
</body>
</html>

<?php
   class MMMR{
        private $strErrMsg;//Error messages from MMMR methods.
        
        /**   When determining the median, mode, and range, the array
         * of numbers input is first sorted. Should the sorting fail,
         * then an error message is logged to this property.
         * 
         * @return string getErrMsg -- Error message.
         */
        public function getErrMsg(){
           return $this->strErrMsg;
        }
       
        /**   Determines if each element of the input array is a number,
         * suitable for use in an arithmetic calculation. Returns true 
         * in this case. Otherwise, returns false.
         * 
         * (This function will be used to handle an invalid request error.)
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return boolean isArrayNumeric -- true, if each element of the input
         *    array is numeric; otherwise, false.
         */
        public function isArrayNumeric($aryNumbers){
           $boolReturnValue = true;
           foreach($aryNumbers as $number){
              if(is_numeric($number) == false){
                 $boolReturnValue = false;
                 break;
              }
           }
           return $boolReturnValue;
        }

        /**   Determines if the input array has elements. Returns true if this is the
         * case; otherwise, returns false.
         * 
         * (This function will be used to handle an invalid request error.)
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return boolean isArrayFull -- true, if the input array contains 
         *    elements--otherwise, false.
         */
        public function isArrayFull($aryNumbers){
           $boolReturnValue = false;
           if(count($aryNumbers) > 0){
              $boolReturnValue = true;
           }
           return $boolReturnValue;
        }

        /**   Calculates and returns the mean of the array of numbers input.
         * Returned value is rounded to three decimal places.
         * 
         * (Note: A division-by-zero condition is not checked by this method,
         *        because it is assumed that the method, "isArrayFull(...)",
         *        would be run first.)
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return number getMean -- The arithmetic average of the numbers
         *    input.
         */
        public function getMean($aryNumbers){
           $sum = 0;
           $count = count($aryNumbers);
           foreach($aryNumbers as $number){
              $sum += $number;
           } 
           return round($sum / $count, 3);
        }

        /**   Determines the median value of the input array of numbers.
         * The median returned is rounded to three decimal places.
         * If no median exists, or can be determined, then Null is returned. 
         * If a problem results when sorting the numbers, then an error message 
         * is logged in a class property.
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return number getMedian -- The middle value of the input array of
         *    numbers; or Null if no middle value exists or can be determined.
         *    (If the input array cannot be sorted, then an error message will 
         *     be logged in a class property.)
         */
        public function getMedian($aryNumbers){
           $returnValue = '';
           $count = count($aryNumbers);
           if($count % 2 != 0){
              if(sort($aryNumbers) == true){
                 $index = ($count - 1) / 2;
                 $returnValue = round($aryNumbers[$index], 3);
              }else{
                 $returnValue = Null;
                 $this->strErrMsg .= 'Error sorting numbers and determining median. ';
              }
           }else{
              $returnValue = Null;
           }
           return $returnValue;
        }

        /**   Determines the mode(s) of the input array of numbers. The return value
         * may be an array, containing those numbers that repeat most often (and the
         * same amount of times). The mode(s) returned are rounded to three decimal 
         * places. The return value could also be Null -- meaning that no modes 
         * were found or could be determined. If a problem results when sorting the 
         * numbers, then an error message is logged in a class property.
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return (array of numbers) getMode -- An array of modes; or Null 
         *   if no mode(s) could be found or determined. (If the input array cannot 
         *   be sorted, then an error message will be logged in a class property.)
         */
        public function getMode($aryNumbers){
           $aryModeContenders = array();
           if(sort($aryNumbers) == true){
              //First create an array of mode contenders
              //(repeated numbers, and how often repeated):
              //-------------------------------------------
              $numberCompare = '';
              $countInstances = 1;
              foreach($aryNumbers as $number){
                 if($number == $numberCompare){
                    $countInstances++;            
                 }else{
                    $aryContender['number'] = $numberCompare;
                    $aryContender['instances'] = $countInstances;
                    $aryModeContenders[] = $aryContender;
                    $countInstances = 1;
                 }
                 $numberCompare = $number;
              }
              //Get greatest number of repeated instances:
              //------------------------------------------
              $greatestInstances = 1;
              foreach($aryModeContenders as $modeContender){
                 if($modeContender['instances'] > $greatestInstances){
                    $greatestInstances = $modeContender['instances'];
                 }
              }
              //Extract modes:
              //--------------
              $aryModes = array();
              if($greatestInstances > 1){
                 foreach($aryModeContenders as $modeContender){
                    if($modeContender['instances'] == $greatestInstances){
                       $aryModes[] = round($modeContender['number'], 3);
                    }
                 }
                 $returnValue = $aryModes;
              //No modes found:
              }else{
                 $returnValue = Null;
              }
           //Problem sorting numbers:
           //------------------------
           }else{
              $returnValue = Null;
              $this->strErrMsg .= 'Error sorting numbers and determining mode. ';
           }
           return $returnValue; 
        }

        /**   Determines the range of values in the array of numbers input.
         * The returned range is rounded to 3 decimal places. If a problem 
         * sorting the array occurs, then Null is returned, and an error mesage 
         * is logged in a class property.
         * 
         * @param array $aryNumbers -- A single-subscripted array of numbers.
         * 
         * @return string getRange -- The difference between the highest and 
         *    lowest numbers in the input array; or Null if the range cannot
         *    be determined. (If the input array cannot be sorted, then an error
         *    message will be logged in a class property.)
         */
        public function getRange($aryNumbers){
            $returnValue = '';
            $count = count($aryNumbers);
            if(sort($aryNumbers) == true){
               $returnValue = round($aryNumbers[$count - 1] - $aryNumbers[0], 3); 
            }else{
               $returnValue = Null;
               $this->strErrMsg .= 'Error sorting numbers and determining range. ';
            }
            return $returnValue;
        }

        /**   Returns the input value, unless that value is Null, in which case
         * an empty string '' is returned. Use this method to format possible
         * Null values for JSON.
         * 
         * @param mixed $value -- Could be an array of numbers, a single number,
         *    or Null.
         * 
         * @return mixed nNullToString -- An array of numbers, a single number,
         *    or ''.
         */
        public function nullToString($value){
           if($value == Null){
              return '';
           }else{
              return $value;
           }
        }
   }//End class MMMR
?>
